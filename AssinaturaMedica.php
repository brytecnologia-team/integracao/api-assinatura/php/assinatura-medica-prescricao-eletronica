<?php

// URL do serviço de inserção de metadados
const URL = 'https://hub2.bry.com.br/pdf/v1/prescricao';
// Token de autenticação gerado no BRy Cloud
const token = 'tokenDeAutenticação';

function adicionaMetadados() {
    $curl = curl_init();

    // Dados referentes ao profissional e o documento que será assinado
    $dados = array(
        // Documento que irá ser assinado
        'prescricao[0][documento]' => new CURLFILE('./arquivos/teste.pdf'),
        // Tipo de prescrição. [Valores disponíveis: MEDICAMENTO, ATESTADO, SOLICITACAO_EXAME,
        // LAUDO, SUMARIA_ALTA, ATENDIMENTO_CLINICO, DISPENSACAO_MEDICAMENTO, VACINACAO e 
        // RELATORIO_MEDICO].
        'prescricao[0][tipo]' => 'MEDICAMENTO',
        // Tipo de profissional. [Valores disponíveis: MEDICO ou FARMACEUTICO]
        'profissional' => 'MEDICO',
        // Número de registro do profissional
        'numeroRegistro' => '123456',
        // Sigla do estado em que o número de registro do profissional está cadastrado.
        'UF' => 'SC',
        // Especialidade do profissional
        'especialidade' => 'ONCOLOGISTA'
    );

    // Cria a requisição que será enviada
    curl_setopt_array($curl, array(
      CURLOPT_URL => URL,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $dados,
      CURLOPT_HTTPHEADER => array(
        "Authorization:" . token
      ),
    ));
  
    $resposta = curl_exec($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if($httpcode == 200) {
        // O retorno será o arquivo com os MetaDados relacionados à prescrição, que agora precisará ser assinado.
        $docComMetadados = fopen("./arquivos/arquivoComMetadados.pdf","wb");
        fwrite($docComMetadados, $resposta);
    } else {
        echo $resposta;
    }

}

adicionaMetadados();